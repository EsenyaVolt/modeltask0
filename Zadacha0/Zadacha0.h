﻿
// Zadacha0.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CZadacha0App:
// Сведения о реализации этого класса: Zadacha0.cpp
//

class CZadacha0App : public CWinApp
{
public:
	CZadacha0App();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CZadacha0App theApp;
