﻿
// Zadacha0Dlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Zadacha0.h"
#include "Zadacha0Dlg.h"
#include "afxdialogex.h"

#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <random>

#define DOTSLOG(x,y) (xplog*((x)-xminlog)),(yplog*((y)-ymaxlog)) // макрос перевода координат для графика сигнала
#define DOTSBIF(x,y) (xpbif*((x)-xminbif)),(ypbif*((y)-ymaxbif)) // макрос перевода координат для графика ошибки

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
// Диалоговое окно CZadacha0Dlg

struct PRNG
{
	mt19937 engine;
};

void initGenerator(PRNG& generator)
{
	// Создаём псевдо-устройство для получения случайного зерна.
	random_device device;
	// Получаем случайное зерно последовательности
	generator.engine.seed(device());
}

double getRandomdouble(PRNG& generator, double minValue, double maxValue)
{
	// Проверяем корректность аргументов
	assert(minValue < maxValue);

	// Создаём распределение
	uniform_real_distribution<double> distribution(minValue, maxValue);

	// Вычисляем псевдослучайное число: вызовем распределение как функцию,
	//  передав генератор произвольных целых чисел как аргумент.
	return distribution(generator.engine);
}

CZadacha0Dlg::CZadacha0Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ZADACHA0_DIALOG, pParent)
	, Xo(0.442)
	, R(3.7)
	, N(200)
	, stepR(0.005)
	, M(100)
	, eps(0.01)
	, R1(0)
	, R2(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CZadacha0Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Xo);
	DDX_Control(pDX, IDC_RADIO1, sluchXo);
	DDX_Text(pDX, IDC_EDIT2, R);
	DDX_Text(pDX, IDC_EDIT3, N);
	DDX_Text(pDX, IDC_EDIT4, stepR);
	DDX_Text(pDX, IDC_EDIT5, M);
	DDX_Text(pDX, IDC_EDIT6, eps);
	DDX_Text(pDX, IDC_EDIT7, R1);
	DDX_Text(pDX, IDC_EDIT8, R2);
}

BEGIN_MESSAGE_MAP(CZadacha0Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CZadacha0Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CZadacha0Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CZadacha0Dlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// Обработчики сообщений CZadacha0Dlg

BOOL CZadacha0Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	//для  картинки
	PicWndLog = GetDlgItem(IDC_LOGMOD);
	PicDcLog = PicWndLog->GetDC();
	PicWndLog->GetClientRect(&PicLog);

	//для  картинки
	PicWndBif = GetDlgItem(IDC_BIFDIAG);
	PicDcBif = PicWndBif->GetDC();
	PicWndBif->GetClientRect(&PicBif);


	// перья
	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		1,						//толщина 1 пиксель
		RGB(0, 0, 0));			//цвет  черный

	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 3 пикселя
		RGB(0, 0, 0));			//цвет черный

	signal_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет синий

	signal2_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		1.5,						//толщина 2 пикселя
		RGB(255, 0, 0));		//цвет red

	signalsgl_pen.CreatePen(			//график
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(155, 0, 255));		//цвет 


	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CZadacha0Dlg::Mashtab(double* arr, int dim, double* mmin, double* mmax)
{
	*mmin = *mmax = arr[0];

	for (int i = 0; i <= dim; i++)
	{
		if (*mmin > arr[i]) *mmin = arr[i];
		if (*mmax < arr[i]) *mmax = arr[i];
	}
}

void CZadacha0Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		PererisovkaLogMod(mnlog, mxlog);
		PererisovkaBifDiag(mnbif, mxbif);
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CZadacha0Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CZadacha0Dlg::PererisovkaLogMod(double y_min, double y_max)
{
	PicDcLog->FillSolidRect(&PicLog, RGB(250, 250, 250));			//закрашиваю фон 

	//ГРАФИК СИГНАЛА

	//область построения
	xminlog = -8;			//минимальное значение х
	xmaxlog = N;			//максимальное значение х
	yminlog = y_min * 1.25;			//минимальное значение y
	ymaxlog = y_max * 1.25;		//максимальное значение y

	xplog = ((double)(PicLog.Width()) / (xmaxlog - xminlog));			//Коэффициенты пересчёта координат по Х
	yplog = -((double)(PicLog.Height()) / (ymaxlog - yminlog));			//Коэффициенты пересчёта координат по У

	PicDcLog->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcLog->MoveTo(DOTSLOG(0, ymaxlog));
	PicDcLog->LineTo(DOTSLOG(0, yminlog));
	//создаём Ось Х
	PicDcLog->MoveTo(DOTSLOG(xminlog, 0));
	PicDcLog->LineTo(DOTSLOG(xmaxlog, 0));

	//подпись осей
	PicDcLog->TextOutW(DOTSLOG(5, ymaxlog - 0.05), _T("S"));
	PicDcLog->TextOutW(DOTSLOG(xmaxlog - 5, 0.1), _T("t"));

	PicDcLog->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (double x = 0; x <= xmaxlog; x += xmaxlog * 0.05)
	{
		PicDcLog->MoveTo(DOTSLOG(x, ymaxlog));
		PicDcLog->LineTo(DOTSLOG(x, yminlog));
	}
	//отрисовка сетки по у
	for (double y = yminlog; y <= ymaxlog; y += ymaxlog * 0.24)
	{
		PicDcLog->MoveTo(DOTSLOG(xminlog, y));
		PicDcLog->LineTo(DOTSLOG(xmaxlog, y));
	}

	//подпись точек на оси
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcLog->SelectObject(font);

	//по Y с шагом 
	for (double i = yminlog; i <= ymaxlog; i += ymaxlog * 0.12)
	{
		CString str;
		str.Format(_T("%.2f"), i);
		PicDcLog->TextOutW(DOTSLOG(-6, i), str);
	}

	//по X с шагом 
	for (double j = xmaxlog * 0.12; j <= xmaxlog; j += xmaxlog * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcLog->TextOutW(DOTSLOG(j, -0.02 * ymaxlog), str);

	}
}

void CZadacha0Dlg::PererisovkaBifDiag(double y_min, double y_max)
{
	PicDcBif->FillSolidRect(&PicBif, RGB(250, 250, 250));			//закрашиваю фон 

	//ГРАФИК СИГНАЛА

	//область построения
	xminbif = -0.17;			//минимальное значение х
	xmaxbif = 4.02;			//максимальное значение х
	yminbif = y_min * 0.8;			//минимальное значение y
	ymaxbif = y_max * 1.25;		//максимальное значение y

	xpbif = ((double)(PicBif.Width()) / (xmaxbif - xminbif));			//Коэффициенты пересчёта координат по Х
	ypbif = -((double)(PicBif.Height()) / (ymaxbif - yminbif));			//Коэффициенты пересчёта координат по У

	PicDcBif->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcBif->MoveTo(DOTSBIF(0, ymaxbif));
	PicDcBif->LineTo(DOTSBIF(0, yminbif));
	//создаём Ось Х
	PicDcBif->MoveTo(DOTSBIF(xminbif, 0));
	PicDcBif->LineTo(DOTSBIF(xmaxbif, 0));

	//подпись осей
	PicDcBif->TextOutW(DOTSBIF(0.1, ymaxbif - 0.05), _T("S"));
	PicDcBif->TextOutW(DOTSBIF(xmaxbif - 0.1, 0.1), _T("t"));

	PicDcBif->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (double x = xminbif; x <= xmaxbif; x += 0.25)
	{
		PicDcBif->MoveTo(DOTSBIF(x, ymaxbif));
		PicDcBif->LineTo(DOTSBIF(x, yminbif));
	}
	//отрисовка сетки по у
	for (double y = 0; y <= ymaxbif; y += 0.19)
	{
		PicDcBif->MoveTo(DOTSBIF(xminbif, y));
		PicDcBif->LineTo(DOTSBIF(xmaxbif, y));
	}

	//подпись точек на оси
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcBif->SelectObject(font);

	//по Y с шагом 
	for (double i = 0; i <= ymaxbif; i += ymaxbif*0.12)
	{
		CString str;
		str.Format(_T("%.1f"), i);
		PicDcBif->TextOutW(DOTSBIF(-0.1, i), str);
	}

	//по X с шагом 
	for (double j = 0.1; j <= xmaxbif; j += 0.25)
	{
		CString str;
		str.Format(_T("%.1f"), j);
		PicDcBif->TextOutW(DOTSBIF(j, -0.02 * ymaxbif), str);

	}
}

double CZadacha0Dlg::Psi()		//рандомизация для шума
{

	double a, b;
	a = 0.0;
	b = 1.0;

	double r = 0;
	for (int i = 1; i <= 12; i++)
	{
		r += (double)rand() / (double)RAND_MAX * (b - a) + a;		// [0;1]
	}
	return r / 12;
}

void CZadacha0Dlg::LogMod(double* arr, int t)
{ 
	for (int i = 0; i <= t; i++)
	{
		arr[i] = 0;
	}

	arr[0] = Xo;

	for (int i = 1; i <= t; i++)
	{
		arr[i] = R * arr[i - 1] * (1 - arr[i - 1]);

	}
}

void CZadacha0Dlg::OnBnClickedButton1()
{
	UpdateData(TRUE);

	if (sluchXo.GetCheck() == BST_CHECKED)
	{
		PRNG generator;
		initGenerator(generator);
		double from = 0;
		double to = 1;

		Xo = getRandomdouble(generator, from, to);
	}

	LogMod(LogModel, N);

	Mashtab(LogModel, N, &mnlog, &mxlog);
	PererisovkaLogMod(-0.1 * mxlog, mxlog);


	PicDcLog->SelectObject(&signal_pen);
	PicDcLog->MoveTo(DOTSLOG(0, LogModel[0]));

	for (int i = 0; i < N; ++i)
	{
		PicDcLog->LineTo(DOTSLOG(i, LogModel[i]));
	}
	

	UpdateData(FALSE);
}

void CZadacha0Dlg::DeleteRepeat(double* arr, int size, int& NewSize)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (fabs(arr[i] - arr[j]) <= eps) // если найден одинаковый элемент
			{
				for (int k = j; k < size - 1; k++)
				{
					// выполнить сдвиг всех остальных элементов массива на -1, начиная со следующего элемента, после найденного дубля
					arr[k] = arr[k + 1];
				}
				size -= 1; // уменьшить размер массива на 1

				if (fabs(arr[i] - arr[j]) <= eps) // если следующий элемент - дубль
				{
					j--; // выполнить переход на предыдущий элемент     
				}
			}
		}
	}

	NewSize = size;
}

void CZadacha0Dlg::OnBnClickedButton2()
{
	UpdateData(TRUE);

	delete[] BifDots;
	double* LogModel_help = new double[1005]{ 0 };
	double* helpX1000 = new double[M] {0};

	vector<double> VectorX;
	vector<double> VectorR;

	VectorX.clear();
	VectorR.clear();

	int sizeNew = 0;

	//ofstream lout("LogMod1000.txt");

	for (double i = 0; i <= 4; i += stepR)
	{
		sizeNew = 0;
		
		for (int j = 0; j < M; j++)
		{
			PRNG generator;
			initGenerator(generator);
			double from = 0;
			double to = 1;

			Xo = getRandomdouble(generator, from, to);
			R = i;
			LogMod(LogModel_help, 1000);

			helpX1000[j] = 0;
			helpX1000[j] = LogModel_help[1000];
			//lout << j << "      " << helpX1000[j] << "      ";
		}

		//lout << endl;

		DeleteRepeat(helpX1000, M, sizeNew);

		for (int j = 0; j <= sizeNew; j++)
		{
			VectorX.push_back(helpX1000[j]);
			VectorR.push_back(i);
		}
	}

	//lout.close();

	razmer = VectorR.size();

	double* BifDotsR = new double[razmer];
	double* BifDotsX = new double[razmer];

	//ofstream out("vectorRX.txt");
	//out << "Значения R" << "\t" << "Значения Х1000" << endl;
	//out << VectorR.size() << "\t" << VectorX.size() << endl;

	for (int i = 0; i < razmer; i++)
	{
		BifDotsR[i] = 0;
		BifDotsX[i] = 0;
		BifDotsR[i] = VectorR[i];
		BifDotsX[i] = VectorX[i];

		//out << i << "\t" << BifDotsR[i] << "\t" << BifDotsX[i] << endl;
	}

	//out.close();

	//Отыскание первой точки бифуркации
	double percent = 4 * 0.75;
	int helpI = 0;

	for (int i = 0; i < razmer; i++)
	{
		if (BifDotsR[i] != BifDotsR[i + 1] && BifDotsR[i + 1] == BifDotsR[i + 2] && BifDotsR[i] <= percent)
		{
			R1 = BifDotsR[i];
			helpI = i;
		}
	}

	//Отыскание второй точки бифуркации
	for (int i = helpI; i < razmer; i++)
	{
		if (BifDotsR[i] == BifDotsR[i + 1] && BifDotsR[i + 2] == BifDotsR[i + 3]
			&& BifDotsR[i + 2] == BifDotsR[i + 4] && BifDotsR[i + 2] != BifDotsR[i]
			&& BifDotsR[i + 4] == BifDotsR[i + 5] && BifDotsR[i + 5] != BifDotsR[i + 6]
			&& BifDotsR[i] == BifDotsR[i - 1])
		{
			R2 = BifDotsR[i];
			break;
		}
	}

	Mashtab(BifDotsX, razmer, &mnbif, &mxbif);
	PererisovkaBifDiag(-0.1 * mxbif, mxbif);

	PicDcBif->SelectObject(&signal_pen);
	PicDcBif->MoveTo(DOTSBIF(BifDotsR[0], BifDotsX[0]));

	for (int i = 0; i < razmer; ++i)
	{
		PicDcBif->Ellipse(DOTSBIF(BifDotsR[i] - 0.002, BifDotsX[i] - 0.002), DOTSBIF(BifDotsR[i] + 0.002, BifDotsX[i] + 0.002));
	}

	BifDots = new BifDiag[razmer];
	for (int i = 0; i < razmer; i++)
	{
		BifDots[i] = { 0, 0, 0 };

		if (i != 0) BifDots[i] = { 0, 0, i - 1 };
		else BifDots[i] = { 0, 0, 0 };
		
	}

	vector<int> VectorIndex;
	VectorIndex.clear();

	int PredIndex = 0;

	for (int i = 0; i < razmer; i++)
	{
		PredIndex = 0;

		if (VectorR[i] != 0.0)
		{
			double x_pred = VectorR[i] - stepR;

			for (int j = 0; j < i; j++)
			{
				if (BifDotsR[j] - x_pred < eps)
				{
					VectorIndex.push_back(j);
				}
			}

			if (VectorIndex.empty() == false)
			{
				if (VectorIndex.size() == 1)
				{
					BifDots[i] = { VectorR[i], VectorX[i], VectorIndex[0] };
				}
				else
				{
					double min = (VectorR[i] - BifDotsR[VectorIndex[0]]) * (VectorR[i] - BifDotsR[VectorIndex[0]])
						+(VectorX[i] - BifDotsX[VectorIndex[0]]) * (VectorX[i] - BifDotsX[VectorIndex[0]]);

					for (int j = 0; j < VectorIndex.size(); j++)
					{
						if (min > (VectorR[i] - BifDotsR[VectorIndex[j]]) * (VectorR[i] - BifDotsR[VectorIndex[j]])
						+ (VectorX[i] - BifDotsX[VectorIndex[j]]) * (VectorX[i] - BifDotsX[VectorIndex[j]]))
						{
							min = ((VectorR[i] - BifDotsR[VectorIndex[j]]) * (VectorR[i] - BifDotsR[VectorIndex[j]])
								+ (VectorX[i] - BifDotsX[VectorIndex[j]]) * (VectorX[i] - BifDotsX[VectorIndex[j]]));

							PredIndex = VectorIndex[j];
						}
					}

					BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
				}
			}
			else
			{
				PredIndex = i - 1;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
		}
		else
		{
			if (i == 0)
			{
				PredIndex = 0;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
			else
			{
				PredIndex = i - 1;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
		}

		VectorIndex.clear();
	}

	UpdateData(FALSE);

	delete[] BifDotsR;
	delete[] BifDotsX;
	delete[] helpX1000;
	delete[] LogModel_help;
}

void CZadacha0Dlg::OnBnClickedButton3()
{
	// TODO: добавьте свой код обработчика уведомлений
	ofstream sout("sortRX.txt");

	PicDcBif->SelectObject(&signal2_pen);

	for (int i = 0; i < razmer; i++)
	{
		PicDcBif->MoveTo(DOTSBIF(BifDots[i].R_diag, BifDots[i].X1000_diag));
		PicDcBif->LineTo(DOTSBIF(BifDots[BifDots[i].index].R_diag, BifDots[BifDots[i].index].X1000_diag));
		sout << i << "\t\t\t" << BifDots[i].R_diag << "\t\t\t" << BifDots[i].X1000_diag << "\t\t\t" << BifDots[i].index << endl;
	}

	sout.close();
}

/*vector<int> IndexDiag;
	int PredIndex = 0;

	for (int i = 0; i < razmer; i++)
	{
		PredIndex = 0;

		if (VectorR[i] != 0.0)
		{
			double x_pred = VectorR[i] - stepR;

			for (int j = 0; j < i; j++)
			{
				if (VectorR[j] - x_pred < eps)
				{
					IndexDiag.push_back(j);
				}
			}

			if (IndexDiag.empty() == false)
			{
				if (IndexDiag.size() == 1)
				{
					BifDots[i] = { VectorR[i], VectorX[i], IndexDiag[0] };
				}
				else
				{
					double min = (VectorR[i] - BifDotsR[IndexDiag[0]]) * (VectorR[i] - BifDotsR[IndexDiag[0]])
						+ (VectorX[i] - BifDotsX[IndexDiag[0]]) * (VectorX[i] - BifDotsX[IndexDiag[0]]);

					for (int j = 0; j < IndexDiag.size(); j++)
					{
						if (min > (VectorR[i] - BifDotsR[IndexDiag[j]]) * (VectorR[i] - BifDotsR[IndexDiag[j]])
							+ (VectorX[i] - BifDotsX[IndexDiag[j]]) * (VectorX[i] - BifDotsX[IndexDiag[j]]))
						{
							min = (VectorR[i] - BifDotsR[IndexDiag[j]]) * (VectorR[i] - BifDotsR[IndexDiag[j]])
								+ (VectorX[i] - BifDotsX[IndexDiag[j]]) * (VectorX[i] - BifDotsX[IndexDiag[j]]);
								PredIndex = IndexDiag[j];
						}
					}

					BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
				}
			}
			else
			{
				PredIndex = i - 1;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
		}
		else
		{
			if (i == 0)
			{
				PredIndex = 0;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
			else
			{
				PredIndex = i - 1;
				BifDots[i] = { VectorR[i], VectorX[i], PredIndex };
			}
		}

		IndexDiag.clear();
	}*/



	/*PicDcBif->SelectObject(&signal2_pen);

	for (int i = 0; i < razmer; i++)
	{
		PicDcBif->MoveTo(DOTSBIF(BifDots[i].R_diag, BifDots[i].X1000_diag));
		PicDcBif->LineTo(DOTSBIF(BifDots[BifDots[i].index].R_diag, BifDots[BifDots[i].index].X1000_diag));
	}*/

	/*double helper = 0;

	for (int i = razmer-1; i >= 1; i--)
	{
		for (int j = 0; j < i; j++)
		{
			if (BifDotsR[j] == BifDotsR[j + 1] && BifDotsX[j] < BifDotsX[j + 1])
			{
				helper = BifDotsX[j];
				BifDotsX[j] = BifDotsX[j + 1];
				BifDotsX[j + 1] = helper;
			}
		}
	}

	ofstream sout("sortRX.txt");
	sout << "Значения R" << "\t" << "Значения Х1000" << endl;

	for (int i = 0; i < razmer; i++)
	{
		sout << i << "\t" << BifDotsR[i] << "\t" << BifDotsX[i] << endl;
	}

	sout.close();

	PicDcBif->SelectObject(&signal2_pen);
	PicDcBif->MoveTo(DOTSBIF(BifDotsR[0], BifDotsX[0]));

	for (int i = 0; i <= helpI; i++)
	{
		PicDcBif->LineTo(DOTSBIF(BifDotsR[i], BifDotsX[i]));
	}
	*/


	//PicDcBif->LineTo(DOTSBIF(BifDotsR[i], BifDotsX[i]));


	/*double help = 0.0;

	for (double i = 4; i >= 0; i -= stepR)
	{
		for (int j = razmer-1; j >= 0; --j)
		{
			if (i == VectorR[j])
			{
				if (VectorX[j - 1] > VectorX[j])
				{
					help = VectorX[j-1];
					VectorX[j - 1] = VectorX[j];
					VectorX[j] = help;
				}
			}
		}
	}

	for (int i = 0; i < razmer; i++)
	{
		out << VectorR[i] << "\t" << VectorX[i] << endl;
	}
	out.close();*/

	/*double helper = 0;

		for (int i = razmer - 1; i >= 1; i--)
		{
			for (int j = 0; j < i; j++)
			{
				if (BifDotsR[j] == BifDotsR[j + 1] && BifDotsX[j] < BifDotsX[j + 1])
				{
					helper = BifDotsX[j];
					BifDotsX[j] = BifDotsX[j + 1];
					BifDotsX[j + 1] = helper;
				}
			}
		}*/