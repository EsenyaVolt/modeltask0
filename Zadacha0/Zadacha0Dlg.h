﻿
// Zadacha0Dlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CZadacha0Dlg
class CZadacha0Dlg : public CDialogEx
{
// Создание
public:
	CZadacha0Dlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ZADACHA0_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double Xo;		double R;		int N;		CButton sluchXo;
	double stepR;	int M;			double eps;
	double R1;		double R2;

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();

	CWnd* PicWndLog;
	CDC* PicDcLog;
	CRect PicLog;

	CWnd* PicWndBif;
	CDC* PicDcBif;
	CRect PicBif;

	//Переменные для работы с масштабом
	double xplog = 0, yplog = 0,			//коэфициенты пересчета
		xminlog = -15, xmaxlog = N,			//максисимальное и минимальное значение х 
		yminlog = -1, ymaxlog = 50;			//максисимальное и минимальное значение y
	double mnlog = -5, mxlog = 50;					//коэффициенты масштабирования

	double xpbif = 0, ypbif = 0,			//коэфициенты пересчета
		xminbif = -1, xmaxbif = 1,			//максисимальное и минимальное значение х 
		yminbif = -0.5, ymaxbif = 5;			//максисимальное и минимальное значение y
	double mnbif = -0.1, mxbif = 1;					//коэффициенты масштабирования

	//объявление ручек
	CPen osi_pen;		// ручка для осей
	CPen setka_pen;		// для сетки
	CPen signal_pen;		// для графика функции
	CPen signal2_pen;		// для графика функции
	CPen signalsgl_pen;		// для графика функции

	afx_msg void PererisovkaLogMod(double y_min, double y_max);
	afx_msg void PererisovkaBifDiag(double y_min, double y_max);
	void Mashtab(double* arr, int dim, double* mmin, double* mmax);
	void DeleteRepeat(double* arr, int size, int& NewSize);
	void LogMod(double* arr, int t);
	double Psi();

	double* LogModel = new double[N + 5];

	struct BifDiag
	{
		double R_diag;					//koord X
		double X1000_diag;				//koord Y
		int index;
	};

	int razmer = 0;
	BifDiag* BifDots;
};
